import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  getRepository,
  JoinColumn,
} from 'typeorm';
import { LotteryEntity } from './Lottery.entity';

@Entity('user_tickets')
export class UserTicketsEntity {
  @PrimaryGeneratedColumn()
  user_ticket_id!: number;

  @Column('int')
  count!: number;

  @Column()
  user_id!: string;

  @JoinColumn({ name: 'lottery_id' })
  @ManyToOne(() => LotteryEntity, (lottery) => lottery.userTickets)
  lottery: LotteryEntity;

  static async addUserToLottery(
    user_id: string,
    lottery: LotteryEntity,
  ): Promise<UserTicketsEntity> {
    return getRepository(UserTicketsEntity).create({
      user_id,
      lottery,
      count: 0,
    });
  }
}
