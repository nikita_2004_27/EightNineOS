import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../../../database/entities/Users.entity';
import { createDescriptionTopText } from '../topVoice';
import './events';

export const topChat = async (message: Message): Promise<void> => {
  const guild = message.guild;
  if (!guild) return;
  const topUsers = await UsersEntity.getTopByColumn('messages', true);
  const description = createDescriptionTopText<UsersEntity>(guild, topUsers, true, 'messages');

  const embed = new MessageEmbed()
    .setColor('#386AFF')
    .setTitle('Топ пользователей по сообщениям за сутки: ')
    .setDescription(description);

  message.channel.send({ embeds: [embed] }).then((msg) => {
    setTimeout(function () {
      msg.delete();
    }, 30000);
  });
};
