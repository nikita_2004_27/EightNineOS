import { Message, MessageEmbed, TextChannel } from 'discord.js';
import { CommandsText } from '../../../logs/commands/text';
import { getConfig } from '../../../config/config';
import './event';

type Report = {
  channelId: string;
  messageID: string;
};

export const reports = new Map<string, Report[]>();

export const report = async (
  message: Message,
  user: null,
  ...reasonStr: string[]
): Promise<void> => {
  if (message.deletable) {
    message.delete();
  }

  const isReportedAlready = reports.has(message.author.id);
  if (isReportedAlready)
    throw new MessageEmbed()
          .setColor('BLUE')
          .setTitle(CommandsText.ALREADY_REPORTED)
          .setFooter('Ожидайте взятия вашей прошлой жалобы');

  const reason = reasonStr.join(' ');
  const target = message.mentions.users.first();

  if (!target || reasonStr.length === 1) {
    throw new MessageEmbed()
          .setColor('RED')
          .setTitle('Ошибка')
          .setDescription(CommandsText.PARAMS_ERROR);
  }

  const embedReport = new MessageEmbed()
    .setColor('#56e01b')
    .setDescription(`${message.author}\n\n**${reason}**\n`)
    .setFooter(message.author.id, message.author.avatarURL()!)
    .setTimestamp();

  const embedReportSent = new MessageEmbed().setColor('BLUE').setTitle(CommandsText.REPORT_SENT);

  const membersFetch = await message.guild!.members.fetch();

  const redactorMembers = membersFetch.filter(
    (member) => member.roles.cache.get(getConfig().roles.redactor) != null,
  );

  const moderatorMembers = membersFetch.filter(
    (member) => member.roles.cache.get(getConfig().roles.moderator) != null,
  );

  const members = new Set([...redactorMembers.values(), ...moderatorMembers.values()]);
  const reportsSent: Report[] = [];

  members.forEach((member) => {
    member
      .send({ embeds: [embedReport] })
      .then((message) => {
        message.react('♿');
        reportsSent.push({
          channelId: message.channel.id,
          messageID: message.id,
        });
      })
      .catch(() => {});
  });

  const reportChannel = message.guild?.channels.cache.get(
    getConfig().channels.text.reportsLog,
  ) as TextChannel;

  reportChannel?.send({ embeds: [embedReport] }).then((message) => {
    reportsSent.push({
      channelId: message.channel.id,
      messageID: message.id,
    });
  });

  reports.set(message.author.id, reportsSent);
  message.author.send({ embeds: [embedReportSent] }).catch(() => {});
};
