import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../../database/entities/Users.entity';
import { createDescriptionTopText } from './topVoice';

export const topCoins = async (message: Message): Promise<void> => {
  const guild = message.guild;
  if (!guild) return;
  const topUsers = await UsersEntity.getTopByColumn('coins', true);
  const description = createDescriptionTopText(guild, topUsers, false);

  const embed = new MessageEmbed()
    .setColor('#386AFF')
    .setTitle('Топ пользователей по коинам: ')
    .setDescription(description);

  message.channel.send({ embeds: [embed] }).then((msg) => {
    setTimeout(function () {
      msg.delete();
    }, 30000);
  });
};
