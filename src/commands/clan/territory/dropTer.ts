import { Message } from 'discord.js';
import { UsersEntity } from '../../../database/entities/Users.entity';
import { TerritoryEntity } from '../../../database/entities/Territory.entity';
import { CommandsText } from '../../../logs/commands/text';

export const dropTer = async (
  message: Message,
  user: UsersEntity,
  terStr: string,
): Promise<void> => {
  const isLeaderOrDeputy = user.clan.isClanLeaderOrDeputy(user);
  if (!isLeaderOrDeputy) return;

  const territoryID = parseInt(terStr);
  if (isNaN(territoryID)) throw CommandsText.PARAMS_ERROR;

  const territory = await TerritoryEntity.getTerritory(territoryID);

  if (user.clan.id === territory.owner) {
    territory.owner = null;
    territory.save();

    message.author.send(`Вы успешно сбросили территорию #${territory.id}`);
  }
};
