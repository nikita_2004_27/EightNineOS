import { Message } from 'discord.js';
import { CommandsText } from '../../logs/commands/text';
import { UsersEntity } from '../../database/entities/Users.entity';
import { sendGeneral, sendMuteLog } from '../../logs/admin/channels';
import { getConfig } from '../../config/config';

const voiceCategories = [
  getConfig().channels.voice.generalVoiceCategory,
  getConfig().channels.voice.clanCategory,
];

export const penalty = async (message: Message): Promise<void> => {
  const target = message.mentions.members?.first();

  if (!target) throw CommandsText.PARAMS_ERROR;
  if (
    !target.voice.channel ||
    !voiceCategories.some((category) => target.voice.channel!.parentId === category)
  )
    throw CommandsText.NOT_CORRECT_VOICE_CHANNEL;

  const targetEntity = await UsersEntity.getOrCreateUser(target.id);

  if (targetEntity.afk_warns === 0) {
    targetEntity.afk_warns++;
    await targetEntity.save();

    await target.voice.setChannel(getConfig().channels.voice.afk);
    target.send('Вы получили устное предупреждение за АФК в общей комнате');
  } else {
    const fine = targetEntity.afk_warns * 500;

    message.author.send(`Вы выдали предупреждение ${target}`);

    if (targetEntity.coins < fine) {
      targetEntity.afk_warns++;
      await target.voice.setChannel(getConfig().channels.voice.afk);
      await targetEntity.takeCoins(targetEntity.coins);
    } else {
      targetEntity.afk_warns++;
      await target.voice.setChannel(getConfig().channels.voice.afk);
      await targetEntity.takeCoins(targetEntity.afk_warns * 500);
    }

    sendMuteLog(`${target} был оштрафован за АФК`);
    sendGeneral(`${target} был оштрафован за АФК в общей комнате`);
    target.send(
      `Вы были оштрафованы за АФК в общей комнате, в следующий раз вы потеряете ${
        ++targetEntity.afk_warns * 500
      }`,
    );
  }
};
