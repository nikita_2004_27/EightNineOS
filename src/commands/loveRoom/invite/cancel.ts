import { Message, MessageEmbed } from 'discord.js';
import { marryInvites } from './marry';
import { CommandsText } from '../../../logs/commands/text';

export const cancelMarry = async (message: Message): Promise<void> => {
  const invite = marryInvites.get(message.author.id);
  const targetID = await marryInvites.get(message.author.id);
  if (!targetID) throw CommandsText.NO_REQUESTS;

  const marryAuthor = await message.guild!.members.fetch(targetID);

  if (invite) {
    message.author.send({
      embeds: [
        new MessageEmbed()
          .setColor(16426470)
          .setDescription(
            `Вы ответили **отказом** на предложение пользователя ${marryAuthor}.\nКажется, вы **разбили** ему сердце... 💔`,
          ),
      ],
    });

    marryAuthor.send({
      embeds: [
        new MessageEmbed()
          .setColor(16426470)
          .setDescription(
            `Пользователь ${message.author} ответил **отказом** на ваше предложение... \nНе отчаивайтесь, ведь это значит, что вы просто еще не нашли нужного вам человека, и всё еще **впереди!**`,
          ),
      ],
    });
    marryInvites.delete(message.author.id);
  }
};
