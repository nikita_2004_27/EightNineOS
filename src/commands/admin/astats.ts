import { Message, MessageEmbed } from 'discord.js';
import { ReddcEntity } from '../../database/entities/Reddc.entity';

export const astats = async (message: Message): Promise<void> => {
  const adminUser = await ReddcEntity.getOrCreateAdmin(message.author.id);
  const embed = new MessageEmbed()
    .setColor('#f4aa42')
    .setTitle('Статистика вашего админ-аккаунта')
    .addField('Баны', adminUser.bans.toString(), true)
    .addField('Варны', adminUser.warns.toString(), true)
    .addField('Муты', adminUser.mutes.toString(), true)
    .addField('Репорты', adminUser.reports.toString(), true)
    .addField('Репорты за неделю', adminUser.week_reports.toString(), true);

  message.author.send({ embeds: [embed] });
};
