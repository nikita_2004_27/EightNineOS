import { client } from './';
import { PenaltiesEntity } from '../database/entities/Penalties.entity';
import { getConfig } from '../config/config';

client.on('guildMemberAdd', async (member) => {
  // deleted due discord anti-spam system
  // const embed = new MessageEmbed()
  //   .setColor(3553599)
  //   .setTitle('Добро пожаловать на 89-SQUAD!')
  //   .setDescription(AdminTexts.JOIN_MEMBER)
  //   .setFooter('Приятного общения!');
  // member.send({ embeds: [embed] }).catch(() => {});

  const isMuted = await PenaltiesEntity.getPenalty(member.id, true);
  if (isMuted) await member.roles.add(getConfig().roles.mute);
  const isMutedChat = await PenaltiesEntity.getPenalty(member.id, false);
  if (isMutedChat) await member.roles.add(getConfig().roles.muteChat);
});
