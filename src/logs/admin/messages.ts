import { client } from '../../client';
import { getConfig } from '../../config/config';
import { GuildChannel, Message, MessageEmbed, PartialMessage, TextChannel } from 'discord.js';

const messageDeleteHandler = async (message: Message | PartialMessage) => {
  if (
    message.channel.type === 'DM' ||
    message.author?.id === client.user?.id ||
    message.guild!.id !== getConfig().guild
  ) {
    return;
  }

  const { author, attachments, content, guild, id, channel } = message;

  const logsChannel = guild!.channels.cache.get(getConfig().channels.text.msgsLogs) as TextChannel;
  if (!logsChannel) return;

  const embed = new MessageEmbed()
    .setAuthor(`${author!.tag} (ID: ${author!.id})`, author!.displayAvatarURL())
    .setDescription(`**\`\`${content ? content.slice(0, 900) : 'Без текста'}\`\`**`)
    .setFooter(`ID: ${id} • #${(channel as GuildChannel).name}`)
    .setTimestamp()
    .setColor('RED');

  logsChannel
    .send({
      embeds: [embed],
      files: Object.values(attachments),
    })
    .catch(() => {});
};

const messageUpdateHandler = async (
  oldMsg: Message | PartialMessage,
  newMsg: Message | PartialMessage,
) => {
  if (!oldMsg.guild || oldMsg.guild.id !== getConfig().guild) return;
  if (oldMsg.author!.id === client.user!.id) return;

  if (oldMsg.content?.length === 0 || newMsg.content?.length === 0) return;

  const { author, guild, channel, id, content: oldContent } = oldMsg;
  const newContent = newMsg.content;

  if (newContent === oldContent) return;

  const logChannel = guild.channels.cache.get(getConfig().channels.text.msgsLogs) as TextChannel;
  if (!logChannel) return;

  const embed = new MessageEmbed()
    .setAuthor(`${author!.tag} (ID: ${author!.id})`, author!.displayAvatarURL())
    .setDescription(
      `**__[Ссылка на сообщение](${oldMsg.url})__\n` +
        `Старое:\n\`\`${oldContent!.slice(0, 900)}\`\`\n` +
        `\nНовое:\n\`\`${newContent!.slice(0, 900)}\`\`**`,
    )
    .setFooter(`ID: ${id} • #${(channel as GuildChannel).name}`)
    .setTimestamp()
    .setColor('BLUE');

  logChannel.send({ embeds: [embed] }).catch(() => {});
};

client.on('messageDelete', messageDeleteHandler);

client.on('messageUpdate', messageUpdateHandler);
