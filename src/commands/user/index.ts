import { Commands } from '../Commands';
import { stats } from './stats';
import { topCoins } from './tops/topCoins';
import { topVoice } from './tops/topVoice';
import { topWeek } from './tops/topWeek';
import { report } from './report/report';
import { transferCoins } from './transferCoins';
import { buyEquip } from './buyEquip';
import { equip } from './equip';
import { newYear } from './newYear';
import { buyLevel } from './buyLevel';
import { buyRole } from './buyrole';
import { buyPng } from './buypng';
import { saleRole } from './saleRole';
import { topChat } from './tops/topChat/topChat';
import { setTicket } from './lottery/setTicket';
import { buyTicket } from './lottery/buyTicket';
import { getConfig } from '../../config/config';
import { bm } from './black-market/bm';
import { voiceLock } from './voice/vlock';
import { voiceUnLock } from './voice/vunlock';
import { voiceSetName } from './voice/vname';
import { vkick } from './voice/vkick';
import { rejPng } from './rejpng';
import { topMod } from './tops/topMod';
import { bmcancel } from './black-market/bmcancel';
import { removeGender } from './removeGender';

export const userCommands: Commands = {
  stats: {
    params: [],
    database: true,
    description: 'Позволяет узнать информацию об аккаунте',
    execute: stats,
  },

  topcoins: {
    params: [],
    database: false,
    description: 'Вывести топ пользователей по коинам',
    execute: topCoins,
  },

  topvoice: {
    params: [],
    database: false,
    description: 'Вывести топ пользователей по времени в войсе',
    execute: topVoice,
  },

  topweek: {
    params: [],
    database: false,
    description: 'Вывести топ пользователей по времени в войсе за неделю',
    execute: topWeek,
  },

  topmod: {
    params: [],
    database: false,
    description: 'Вывести топ пользователей по времени в войсе за неделю',
    execute: topMod,
  },

  report: {
    description: 'Подать жалобу на пользователя',
    database: false,
    params: ['@пользователь', 'причина'],
    execute: report,
  },

  gc: {
    description: 'Передать коины другому пользователю',
    database: true,
    params: ['@пользователь', 'количество'],
    execute: transferCoins,
  },

  equip: {
    description: 'Узнать информацию о персонаже',
    database: true,
    params: [],
    execute: equip,
  },

  buyequip: {
    description: 'Покупка улучшений на свой аккаунт',
    database: true,
    params: ['equip'],
    execute: buyEquip,
  },

  ny: {
    description: 'Сколько до Нового Года?',
    database: false,
    params: [],
    execute: newYear,
  },

  up: {
    description: '',
    database: true,
    params: [],
    execute: buyLevel,
  },

  buyrole: {
    description: 'buyrole',
    database: true,
    params: [],
    execute: buyRole,
  },

  salerole: {
    description: 'salerole',
    database: true,
    params: [],
    execute: saleRole,
  },

  buypng: {
    description: 'оформить подписку на роль для вложений',
    params: [''],
    allowedRoles: [],
    database: true,
    execute: buyPng,
  },

  rejpng: {
    description: 'отказатья от подписки на роль для вложений',
    params: [''],
    allowedRoles: [],
    database: true,
    execute: rejPng,
  },

  topchat: {
    description: 'Вывести топ пользователей по сообщениям в чате за день',
    database: false,
    params: [],
    execute: topChat,
  },

  setticket: {
    description: 'Создать лотерею',
    database: false,
    params: ['Кол-во мест', 'Цена билета'],
    allowedRoles: [getConfig().roles.moderator],
    execute: setTicket,
  },

  buyticket: {
    description: 'Купить лотерейный билет',
    database: true,
    params: ['Кол-во билетов'],
    execute: buyTicket,
  },

  bm: {
    description: 'Черный рынок',
    database: true,
    params: ['Кол-во коинов', 'Описание услуги'],
    execute: bm,
  },

  bmcancel: {
    description: 'Удаление объявления с чёрного рынка',
    database: false,
    params: ['Айди сообщения'],
    execute: bmcancel,
  },

  vlock: {
    description: 'Закрыть комнату',
    params: ['@пользователь (если нет - то от всех)'],
    allowedRoles: [],
    database: false,
    execute: voiceLock,
  },

  vunlock: {
    description: 'Открыть комнату',
    params: ['@пользователь (если нет - то для всех)'],
    allowedRoles: [],
    database: false,
    execute: voiceUnLock,
  },

  vname: {
    description: 'Установить новое имя каналу',
    params: ['Имя'],
    allowedRoles: [],
    database: false,
    execute: voiceSetName,
  },

  vkick: {
    description: 'кик из войса',
    params: [''],
    allowedRoles: [],
    database: false,
    execute: vkick,
  },

  rg: {
    description: 'снять гендерку',
    params: [''],
    allowedRoles: [getConfig().roles.man, getConfig().roles.woman],
    database: true,
    execute: removeGender,
  },
};
