import {
  Column,
  Entity,
  getRepository,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ClansEntity } from './Clans.entity';

@Entity('clan_territory')
export class TerritoryEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  name!: string;

  @Column()
  price!: number;

  @Column('double')
  income!: number;

  @JoinColumn({ name: 'owner' })
  @ManyToOne(() => ClansEntity, (clan) => clan.territories)
  owner!: number | ClansEntity | null;

  @Column()
  freeze!: boolean;

  static getTerritory(id: number): Promise<TerritoryEntity> {
    return getRepository(TerritoryEntity).findOne(id, {
      loadRelationIds: true,
    }) as Promise<TerritoryEntity>;
  }

  static getTerritoriesList(): Promise<TerritoryEntity[]> {
    return getRepository(TerritoryEntity).find({
      relations: ['owner'],
      order: { id: 'ASC' },
      cache: 60000,
    });
  }

  save(): Promise<TerritoryEntity> {
    return getRepository(TerritoryEntity).save(this);
  }
}
