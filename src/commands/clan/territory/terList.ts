import { Message, MessageEmbed } from 'discord.js';
import { TerritoryEntity } from '../../../database/entities/Territory.entity';
import { ClansEntity } from '../../../database/entities/Clans.entity';

export const terList = async (message: Message): Promise<void> => {
  const territories = await TerritoryEntity.getTerritoriesList();

  const top = territories.map((territory) => {
    let text = '';

    const clanName = (territory.owner as ClansEntity)?.name ?? 'Государство';

    text += `${territory.id}. **${territory.name}** под владением клана **${clanName}**`;
    text += territory.freeze ? ' - __заморожена__\n\n' : '\n\n';

    return text;
  });

  const embed = new MessageEmbed()
    .setColor('#386AFF')
    .setTitle('Территории: ')
    .setDescription(top.join(''));

  message.author.send({ embeds: [embed] });
};
