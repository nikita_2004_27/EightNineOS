import { Message, MessageEmbed } from 'discord.js';
import { createDescriptionTopText } from './topVoice';
import { UsersEntity } from '../../../database/entities/Users.entity';

export const topWeek = async (message: Message): Promise<void> => {
  const guild = message.guild;
  if (!guild) return;
  const topWeek = await UsersEntity.getTopByColumn('voice_time_week', false);
  const description = createDescriptionTopText<UsersEntity>(guild, topWeek, false);

  const embed = new MessageEmbed()
    .setColor('#386AFF')
    .setTitle('Топ пользователей по времени в войсе за неделю: ')
    .setDescription(description);

  message.channel.send({ embeds: [embed] }).then((msg) => {
    setTimeout(function () {
      msg.delete();
    }, 30000);
  });
};
