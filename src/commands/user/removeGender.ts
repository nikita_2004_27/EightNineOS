import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../database/entities/Users.entity';
import { getConfig } from '../../config/config';
import { CommandsText } from '../../logs/commands/text';

export const removeGender = async (message: Message, user: UsersEntity): Promise<void> => {
  const targetMember = await message.guild?.members.fetch(message.author.id);
  if (!targetMember) return;

  if (user.coins < getConfig().removeGenderPrice) {
    throw new MessageEmbed()
      .setTitle('Ошибка')
      .setColor('RED')
      .setDescription(CommandsText.NO_COINS);
  }

  targetMember.roles.cache.has(getConfig().roles.man)
    ? targetMember.roles.remove(getConfig().roles.man)
    : targetMember.roles.remove(getConfig().roles.woman);

  targetMember.send({
    embeds: [new MessageEmbed().setTitle(CommandsText.SUCCESS_REMOVE).setColor('BLUE')],
  });

  await user.takeCoins(getConfig().removeGenderPrice);
};
