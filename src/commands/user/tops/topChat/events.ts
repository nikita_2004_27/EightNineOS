import { client } from '../../../../client';
import { Message } from 'discord.js';
import { UsersEntity } from '../../../../database/entities/Users.entity';
import { getConfig } from '../../../../config/config';

type MessageCounter = {
  user_id: string;
  count: number;
};

let messages: MessageCounter[] = [];

client.on('messageCreate', (message: Message) => {
  if(message.author.bot) return;
  if(message.content[0] === '!') return;

  if (message.channel.id === getConfig().channels.text.general) {
    const messageCount = messages.find((msg) => msg.user_id === message.author.id);

    if (messageCount) {
      messageCount.count++;
    } else {
      messages.push({
        count: 1,
        user_id: message.author.id,
      });
    }
  }
});

const updateDBMessage = async () => {
  for (const messageCount of messages) {
    const user = await UsersEntity.getOrCreateUser(messageCount.user_id);

    user.messages += messageCount.count;
    user.save();
  }

  resetMessages();
};

export const updateDBMessageCron = (): void => {
  setInterval(updateDBMessage, 180000);
};

export const resetMessages = (): void => {
  messages = [];
};
