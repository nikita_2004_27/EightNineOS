import { Message } from 'discord.js';
import { UsersEntity } from '../../../database/entities/Users.entity';

export const clanname = async (
  message: Message,
  user: UsersEntity,
  ...nameArr: string[]
): Promise<void> => {
  console.log(user.clan);
  if (!user.clan.isClanLeaderOrDeputy(user)) return;

  const name = nameArr.join(' ');
  if (name.length > 40) return;

  const role = await message.guild!.roles.fetch(user.clan.role);
  role?.setName(name);

  user.clan.name = name;
  user.clan.save();
};
