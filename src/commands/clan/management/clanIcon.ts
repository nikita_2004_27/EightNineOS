import { Base64Resolvable, BufferResolvable, EmojiResolvable, Message } from 'discord.js';
import { UsersEntity } from '../../../database/entities/Users.entity';

export const clanIcon = async (
  message: Message,
  user: UsersEntity,
  picture: BufferResolvable | Base64Resolvable | EmojiResolvable,
): Promise<void> => {
  if (!user.clan.isClanLeaderOrDeputy(user)) return;

  const role = await message.guild!.roles.fetch(user.clan.role);
  role?.setIcon(picture);
};
