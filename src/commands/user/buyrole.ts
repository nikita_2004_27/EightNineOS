import { Message, MessageEmbed } from 'discord.js';
import { getRepository } from 'typeorm';
import { ShopEntity } from '../../database/entities/Shop.entity';
import { CommandsText } from '../../logs/commands/text';
import { UsersEntity } from '../../database/entities/Users.entity';

export const buyRole = async (
  message: Message,
  user: UsersEntity,
  roleStr: string,
): Promise<void> => {
  const targetMember = await message.guild!.members.fetch(message.author.id);
  if (!targetMember) return;

  if (!roleStr)
    throw new MessageEmbed()
        .setTitle('Ошибка')
        .setColor("RED")
        .setDescription("Пожалуйста, выберите роль и напишите её номер")
        .setImage('https://cdn.discordapp.com/attachments/601856470450962436/903401818913116181/roles_redesign.png');

  const role = parseInt(roleStr);
  if (isNaN(role)) throw CommandsText.PARAMS_ERROR;

  const repository = getRepository(ShopEntity);
  const roleToBuy = await repository.findOne({ id: role });
  if (!roleToBuy) throw CommandsText.ROLE_DOES_NOT_EXIST;

  if (targetMember.roles.cache.has(roleToBuy.roleId)) throw CommandsText.BUY_ROLE_AGAIN

  const groupRoles = (await repository.find({ where: { group: roleToBuy.group } })).map(
    (role) => role.roleId,
  );

  if (
    targetMember.roles.cache.map((role) => role.id).some((_) => groupRoles.includes(_)) &&
    roleToBuy.group != 5 &&
    roleToBuy.group != 6 &&
    roleToBuy.group != 7
  ) throw CommandsText.USER_HAS_ROLE_FROM_THIS_GROUP;

  if (roleToBuy.price <= user.coins) {
    const serverRole = message.guild!.roles.cache.get(roleToBuy.roleId);
    const embed = new MessageEmbed()
      .setColor('BLUE')
      .setTitle(`Вы успешно приобрели роль **${serverRole?.name}**`);
    user.takeCoins(roleToBuy.price);
    message.author.send({ embeds: [embed] });
    targetMember.roles.add(roleToBuy.roleId);
  } else throw CommandsText.NO_COINS;
};
