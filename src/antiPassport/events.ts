import { VoiceState } from 'discord.js';
import { client } from '../client';
import { getConfig } from '../config/config';

client.on('voiceStateUpdate', async (_: VoiceState, newVoice: VoiceState) => {
  const targetMember = await newVoice.guild!.members.fetch(newVoice.id);
  if (!targetMember) return;
  if (targetMember.roles.cache.has(getConfig().roles.mute)) {
    targetMember.voice.disconnect().catch(() => {});
    return;
  }

  if (newVoice.channel?.id === getConfig().channels.voice.antiPassport) {
    const targetMember = newVoice.member!;

    if (targetMember.roles.cache.has(getConfig().roles.passport)) {
      await targetMember.roles.remove(getConfig().roles.passport);
    }

    if (targetMember.roles.cache.has(getConfig().roles.hours750)) {
      await targetMember.roles.remove(getConfig().roles.hours750);
    }

    await targetMember.voice.disconnect();
  }
});
