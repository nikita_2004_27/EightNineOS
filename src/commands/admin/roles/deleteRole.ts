import { Message } from 'discord.js';
import { getRepository } from 'typeorm';
import { RoleEntity } from '../../../database/entities/Role.entity';
import { sendRoleLog } from '../../../logs/admin/channels';

export const deleteRole = async (
  message: Message,
  user: null,
  toRemoveAccessRoleId: string,
  roleToChangeSetsId: string,
): Promise<void> => {
  const toRemoveAccessRole = message.guild?.roles.cache.get(toRemoveAccessRoleId);
  if (!toRemoveAccessRole) return;

  const toChangeRole = message.guild?.roles.cache.get(roleToChangeSetsId);
  if (!toChangeRole) return;

  const roleDoc = await getRepository(RoleEntity).findOne({
    role_id: toChangeRole.id,
  });

  if (!roleDoc) return;

  const indexOfId = roleDoc.access_roles.indexOf(toRemoveAccessRoleId);
  if (indexOfId === -1) return;

  roleDoc.access_roles.splice(indexOfId, 1);

  await roleDoc.save();

  const text = `**Доступ к роли \`${toChangeRole.name}\` были убран у роли \`${toRemoveAccessRole.name}\`**`;

  sendRoleLog(
    `${message.author.tag} забирает доступ к роли <@&${toChangeRole.id}> участникам с ролью <@&${toRemoveAccessRole.id}>`,
  );

  message.author.send(text).catch(() => null);
};
