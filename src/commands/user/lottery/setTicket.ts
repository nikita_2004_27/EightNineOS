import { Message } from 'discord.js';
import { UsersEntity } from '../../../database/entities/Users.entity';
import { setLottery } from './util';
import { CommandsText } from '../../../logs/commands/text';

export const setTicket = async (
  message: Message,
  user: UsersEntity,
  priceStr: string,
  placesStr: string,
): Promise<void> => {
  const price = parseInt(priceStr);
  const places = parseInt(placesStr);

  if (isNaN(price) || isNaN(places) || places <= 0 || price <= 0) throw CommandsText.PARAMS_ERROR;

  message.author.send(`Следующая лотерея будет на ${places} слотов по ${price} каждый!`);
  setLottery(places, price);
};
