import { Message, MessageEmbed } from 'discord.js';
import { getRepository } from 'typeorm';
import { ClansEntity } from '../../database/entities/Clans.entity';

export const topClans = async (message: Message): Promise<void> => {
  const topClans = await getRepository(ClansEntity).find({ order: { coins: 'DESC' }, take: 10 });

  const topText = topClans
    .map((clan, i) => {
      let text = '';
      i++;

      switch (i) {
        case 1:
          text = `:first_place: `;
          break;
        case 2:
          text = `:second_place: `;
          break;
        case 3:
          text = `:third_place: `;
          break;
      }

      text += `** ${i}. ** ${clan.name}`;
      return text;
    })
    .join('\n\n');

  const embed = new MessageEmbed()
    .setColor('#386AFF')
    .setTitle('Топ кланов по коинам: ')
    .setDescription(topText);

  message.channel.send({ embeds: [embed] }).then((msg) => {
    setTimeout(msg.delete, 30000);
  });
};
