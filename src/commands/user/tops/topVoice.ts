import { Guild, Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../../database/entities/Users.entity';

export const topVoice = async (message: Message): Promise<void> => {
  const guild = message.guild;
  if (!guild) return;
  const topUsers = await UsersEntity.getTopByColumn('voice_time', false);
  const description = createDescriptionTopText<UsersEntity>(guild, topUsers, false);

  const embed = new MessageEmbed()
    .setColor('#386AFF')
    .setTitle('Топ пользователей по времени в войсе: ')
    .setDescription(description);

  message.channel.send({ embeds: [embed] }).then((msg) => {
    setTimeout(function () {
      msg.delete();
    }, 30000);
  });
};

export const createDescriptionTopText = <T extends UsersEntity>(
  guild: Guild,
  users: Partial<T>[],
  showKey: boolean,
  key?: keyof T,
): string => {
  return users
    .map((user, i) => {
      const memberName = `<@${user.user_id}>`;

      let text = '';
      i++;

      switch (i) {
        case 1:
          text = `:first_place: `;
          break;
        case 2:
          text = `:second_place: `;
          break;
        case 3:
          text = `:third_place: `;
          break;
      }

      text += `** ${i}. ** ${memberName}${showKey ? ` - ${user[key as keyof T]}` : ''}`;
      return text;
    })
    .join('\n\n');
};
