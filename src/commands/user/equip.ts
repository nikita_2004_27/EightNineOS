import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../database/entities/Users.entity';

export const equip = (message: Message, user: UsersEntity): void => {
  const embed = new MessageEmbed()
    .setColor('#00AE86')
    .setTitle(`Статистика персонажа ${message.author.tag}`)
    .addField('Шлем', user.equipment[1] + ' уровень', true)
    .addField('Нагрудник', user.equipment[2] + ' уровень', true)
    .addField('Поножи', user.equipment[3] + ' уровень', true)
    .addField('Сапоги', user.equipment[4] + ' уровень', true)
    .addField('Меч', user.equipment[5] + ' уровень', true)
    .addField('Лук', user.equipment[6] + ' уровень', true)
    .setFooter(`Общая сила персонажа: ${user.getEquipment()}`);

  message.author.send({ embeds: [embed] });
};
