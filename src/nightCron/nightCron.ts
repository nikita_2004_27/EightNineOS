import { CronJob } from 'cron';
import { getRepository } from 'typeorm';
import { UsersEntity } from '../database/entities/Users.entity';
import { resetMessages } from '../commands/user/tops/topChat/events';
import { TerritoryEntity } from '../database/entities/Territory.entity';
import { ClansEntity } from '../database/entities/Clans.entity';
import _ from 'lodash';
import { getConfig } from '../config/config';
import { client } from '../client';
import moment from 'moment';
import { MessageEmbed, TextChannel } from 'discord.js';
import { CommandsText } from '../logs/commands/text';
import { LoveRoomsEntity } from '../database/entities/LoveRooms.entity';
import { LoveRoomsChildrenEntity } from '../database/entities/LoveRoomsChildren.entity';
import { BlackMarketEntity } from '../database/entities/BlackMarket.entity';

const nightTimer = async () => {
  const guild = await client.guilds.fetch(getConfig().guild);

  const userRepository = getRepository(UsersEntity);
  const territoryRepository = getRepository(TerritoryEntity);
  const clanRepository = getRepository(ClansEntity);
  const loveRoomRepository = getRepository(LoveRoomsEntity);
  const childrenRepository = getRepository(LoveRoomsChildrenEntity);
  const blackMarketRepository = getRepository(BlackMarketEntity);

  await userRepository.createQueryBuilder().update().set({ messages: 0 }).execute();
  resetMessages();

  await territoryRepository.createQueryBuilder().update().set({ freeze: false }).execute();

  const membersFetch = await guild.members.fetch();
  const members = membersFetch.filter(
    (member) => member.roles.cache.get(getConfig().roles.png) != null,
  );
  const dateNow = moment();

  members.forEach(async (member) => {
    const user = await UsersEntity.getOrCreateUser(member.id);
    const canProlongSubscribe = dateNow.isAfter(user.png_subscribe_timeout);
    if (canProlongSubscribe) {
      if (user.coins >= 2000) {
        user.png_subscribe_timeout = moment().add(7, 'day').toDate();
        user.takeCoins(2000);
        member.send({
          embeds: [new MessageEmbed().setColor('BLUE').setTitle('Подписка была продлена')],
        });
      } else {
        member.roles.remove(getConfig().roles.png);
        user.png_subscribe_timeout = null;
        member.send({
          embeds: [
            new MessageEmbed()
              .setColor('RED')
              .setTitle('Подписка была отменена')
              .setDescription(CommandsText.NO_COINS),
          ],
        });
      }
    }
  });

  const clans = await clanRepository.find();

  for (const clan of clans) {
    const territories = await territoryRepository.find({ where: { owner: clan.id } });

    if (!_.isEmpty(territories)) {
      for (const { income } of territories) {
        clan.coins += income;
      }
    }

    const usersCount = await clan.countMembers();
    const clanPrice = getConfig().clanExtendPrice + usersCount * 300;
    const isGotEnoughMoney = clan.coins >= clanPrice;

    if (isGotEnoughMoney) {
      clan.coins -= clanPrice;
      await clan.save();
    } else {
      clan.deleteClan();
    }
  }

  const loveRooms = await loveRoomRepository.find();

  for (const room of loveRooms) {
    if (dateNow.diff(moment(room.last_payment_date), 'month') < 1) continue;

    if (room.balance < getConfig().loveRoomExtendPrice) {
      const children = await childrenRepository.find({
        where: [{ room_id: room.room_id }],
      });

      for (const child of children) {
        const childMember = await guild?.members.fetch(child.user_id);
        if (!childMember) return;

        childMember.roles.add(getConfig().roles.orphanage);
        await childrenRepository.delete(child);
      }
      room.sendToAllMembers(
        new MessageEmbed().setColor('RED').setDescription(CommandsText.NO_COINS_ON_BALANCE),
      );
      await loveRoomRepository.delete(room);
      await room.deleteRoom();
      continue;
    }
    await room.takeCoins();
  }

  const orphanages = membersFetch.filter(
    (member) => member.roles.cache.get(getConfig().roles.orphanage) != null,
  );
  const accrual = getConfig().orphanageExtendAccrual / moment().daysInMonth();

  orphanages.forEach(async (member) => {
    const user = await UsersEntity.getOrCreateUser(member.id);
    user.coins += accrual;
    member.send({
      embeds: [new MessageEmbed().setColor('BLUE').setTitle('Вам начислено пособие детдомовца')],
    });
    await user.save();
  });

  const blackMarketMembers = await blackMarketRepository.find();

  const blackMarketPrice = getConfig().blackMarketPrice;

  for (const blackMarketMember of blackMarketMembers) {
    const user = await UsersEntity.getOrCreateUser(blackMarketMember.user_id);
    const member = await guild.members.fetch(user.user_id);
    if (user.coins >= 250) {
      user.coins -= blackMarketPrice;
    } else {
      const channel = guild.channels.cache.get(
        getConfig().channels.text.blackMarket,
      ) as TextChannel;
      if (!channel) return;

      await channel.messages.delete(blackMarketMember.message_id);
      await blackMarketRepository.delete(blackMarketMember);
      await member.send({
        embeds: [
          new MessageEmbed()
            .setColor('RED')
            .setTitle('Ваше объявление было удалено')
            .setDescription(CommandsText.NO_COINS_FOR_EXTENSION),
        ],
      });
    }
    await user.save();
  }
};

export const nightCron = new CronJob('30 0 0 * * *', nightTimer);
