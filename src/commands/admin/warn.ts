import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../database/entities/Users.entity';
import { ReddcEntity } from '../../database/entities/Reddc.entity';
import { AdminTexts } from '../../logs/admin/text';
import { sendGeneral, sendWarnLog } from '../../logs/admin/channels';
import { getConfig } from '../../config/config';
import { CommandsText } from '../../logs/commands/text';

export const warn = async (
  message: Message,
  user: null,
  _: string,
  ...reasonArr: string[]
): Promise<void> => {
  const target = message.mentions.users.first();
  const reason = reasonArr.join(' ');

  if (!target || !reason) throw CommandsText.PARAMS_ERROR;

  const targetMember = await message.guild!.members.fetch(target.id);
  if (!targetMember) return;
  if (getConfig().roles.admins.some((roleID) => targetMember.roles.cache.has(roleID))) return;

  const targetUser = await UsersEntity.getOrCreateUser(target.id);
  targetUser.warns++;

  const embedBan = new MessageEmbed()
    .setColor('RED')
    .setTitle('О-паньки!?!?! ')
    .setDescription(AdminTexts.BAN_USER)
    .setFooter('Причина бана: 3/3 варнов');

  const embedWarn = new MessageEmbed()
    .setColor('RED')
    .setTitle('Вам выдано предупреждение')
    .addField('Причина', reason, true)
    .addField('Количество варнов', targetUser.warns + '/3', true);

  sendGeneral(`${target} получил варн по причине: "**${reason}**"`);
  sendWarnLog(
    `${message.author} выдал варн пользователю ${target} - ${targetUser.warns}/3\nПричина: ${reason}`,
  );
  await target.send({ embeds: [embedWarn] });

  if (targetUser.warns >= 3) {
    await target.send({ embeds: [embedBan] });
    targetMember.ban({ days: 1, reason: '3 варна' });

    targetUser.warns = 0;
  }

  targetUser.save();
  ReddcEntity.addStat(message.author, { warns: 1 });
};
