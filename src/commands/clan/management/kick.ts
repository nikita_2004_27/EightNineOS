import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../../database/entities/Users.entity';
import { CommandsText } from '../../../logs/commands/text';
import { sendClanLog, sendGeneral } from '../../../logs/admin/channels';
import moment from 'moment';

export const kickClan = async (message: Message, user: UsersEntity): Promise<void> => {
  const targetMember = message.mentions.members!.first();
  if (!targetMember)
    throw CommandsText.PARAMS_ERROR;

  if (!user.clan.isClanLeaderOrDeputy(user)) throw CommandsText.USAGE_ONLY_FOR_LEADER;

  if (message.author.id === targetMember.id) throw CommandsText.CANNOT_SELF_KICK;

  if (user.clan.leader === targetMember.id) throw CommandsText.CANNOT_KICK_LEADER;

  if (user.clan.deputy === targetMember.id) {
    // @ts-ignore
    user.clan.deputy = null;
    await user.clan.save();
  }

  const targetUser = await UsersEntity.getOrCreateUser(targetMember.id);
  if (targetUser.clan !== user.clan.id) return;

  targetMember.roles.remove(user.clan.role);

  targetUser.clan_join_timeout = moment().add(1, 'day').toDate();
  // @ts-ignore
  targetUser.clan = null;
  await targetUser.save();

  const embed = new MessageEmbed()
    .setColor('BLUE')
    .setTitle(`Вы были исключены из клана ${user.clan.name}`);

  targetMember.send({ embeds: [embed] });
  sendGeneral(`${targetMember} был выгнан(а) из клана "**${user.clan.name}**"`);
  sendClanLog(`${message.member} выгнал с клана ${targetMember} клан "**${user.clan.name}**"`);

  const clanMembersCount = await user.clan.countMembers();
  if (clanMembersCount < 7) user.clan.clanChannel(message.guild!, false);
};
