import { MessagesEntity } from '../database/entities/Messages.entity';

export const pastasOrder: string[] = [];

export const generatePastasOrder = async (): Promise<void> => {
  const dbPastas = await MessagesEntity.repository.find();

  pastasOrder.push(...dbPastas.map((m) => m.message));
};
