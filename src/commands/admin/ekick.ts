import { Message } from 'discord.js';
import { sendEventsLog } from '../../logs/admin/channels';

export const ekick = async (
  message: Message,
  user: null,
  _: string,
  ...reason: string[]
): Promise<void> => {
  const target = message.mentions.members?.first();

  // if (!target || !reason) throw CommandsText.PARAMS_ERROR;
  // if (!target.voice.channel || target.voice.channel.parentId !== getConfig().channels.voice.events)
  //   throw CommandsText.NOT_CORRECT_VOICE_CHANNEL;
  //
  // target.voice.kick();
  // target.voice.channel.updateOverwrite(target, {
  //   CONNECT: false,
  // });

  message.channel.send({ content: reason.join(' ') });

  sendEventsLog(`!ewarn ${target} ${reason.join(' ')}`);
};
