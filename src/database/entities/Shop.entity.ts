import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('role_shop')
export class ShopEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('text')
  roleId: string;

  @Column('int')
  price: number;

  @Column('int')
  voiceReq: number;

  @Column('int')
  group: number;
}
